\chapter{Use-Cases}\label{ch:use-cases}
The functionality of the implemented task minimizer is illustrated in this chapter, based on three use-cases.
These use-cases are a selection from bug-reports of Fast Downward users and from the planner's official list of issues\footnote{http://issues.fast-downward.org/}.
The fact that these bugs are considered resolved at the time of writing of this report is irrelevant, since the only goal is to reproduce them on the respective version of the planner.
We executed the test runs of the minimizer on a laptop computer with the following specifications:
\begin{itemize}
    \item Operating system: Ubuntu 20.04
    \item Processor: Intel i5-5300U, 2 cores, 2.3 GHz base frequency
    \item Memory: 11.4 GiB
\end{itemize}

We believe memory bottlenecks occurred in some runs, which would explain longer running times, e.g., with delete option \textit{predicate (truth)} in Table~\ref{tab:avg-results-issue335}.
We chose to ignore predicate deletion options \textit{truth} and \textit{falsity} in test runs where all three PDDL transformation options were used, to keep the number of test runs reasonable.
The averages and standard deviations are computed from 5 runs of each configuration.
Since we observed a high volatility in running times even with a fixed randomization seed, we used the fixed seed 42 for all test runs.
For a cleaner appearance, throughout this entire chapter, full file paths (or parts of them) are aliased with \texttt{/path/to/}.

\section{Use-Case 1: Issue 335}\label{sec:use-case-1:-issue-335}
This issue was used to develop and test the PDDL version of the task minimizer, despite it being the most outdated one.
It has been reported in May 2012 and was marked as resolved in August 2014~\cite{issue335}.
The planning task triggering the bug is described in the files \texttt{cntr-domain.pddl} and \texttt{cntr-problem.pddl}.
The task requires the ADL fragment of PDDL and the initially parsed task instance features 46 actions, 62 objects and 29 predicate symbols.
On the revision of Fast Downward with commit hash \texttt{09ccef5fd[...]} (from 04/08/2014), which is the parent commit of the one where the bug was fixed, the following error can be reproduced when executing \texttt{src/translate/translate.py} with the two above mentioned files:

\begin{lstlisting}[label={lst:lstlisting1},frame=single]
Traceback (most recent call last):
  File "translate/translate.py", line 677, in <module>
    main()
  File "translate/translate.py", line 667, in main
    sas_task = pddl_to_sas(task)
  File "translate/translate.py", line 540, in pddl_to_sas
    implied_facts)
  File "translate/translate.py", line 422, in translate_task
    actions, axioms, goals)
  File "/path/to/downward/src/translate/axiom_rules.py", line 11, in handle_axioms
    axioms = compute_negative_axioms(axioms_by_atom, axiom_literals)
  File "/path/to/downward/src/translate/axiom_rules.py", line 158, in compute_negative_axioms
    new_axioms += negate(axioms_by_atom[literal.positive()])
  File "/path/to/downward/src/translate/axiom_rules.py", line 168, in negate
    assert len(condition) > 0, "Negated axiom impossible; cannot deal with that"
AssertionError: Negated axiom impossible; cannot deal with that
\end{lstlisting}

The last line of the error message (\textit{AssertionError: Negated axiom impossible; cannot deal with that}) can be used as the characteristic string for the minimizer.
In Table~\ref{tab:avg-results-issue335}, we illustrate the average running times and the resulting minimized task elements for a set of configurations.
The minimizer command we use for the tests (with the respective delete option) is the following:

\begin{lstlisting}[label={lst:command-tests-issue335},frame=single]
python3 /path/to/minimizer.py --problem "python3.7 /path/to/downward/src/translate/translate.py /path/to/cntr-domain.pddl /path/to/cntr-problem.pddl" --characteristic "AssertionError: Negated axiom impossible; cannot deal with that" --delete predicate object action
\end{lstlisting}



\begin{table}[H]
    \centering
    \begin{tiny}
        \makebox[\textwidth]{
        \begin{tabular}{| *{8}{c|}}
            \hline
            & & \multicolumn{3}{c|}{time per option} & \multicolumn{3}{c|}{$\text{before}\rightarrow\text{after}$} \\
            delete option(s)          & total time            & action              & object             & predicate             & actions           & objects           & predicates        \\
            \hline\hline
            action                    & $251.087\pm6.869$s    & $251.087\pm6.870$s  &                    &                       & $46\rightarrow4$  & $62\rightarrow62$ & $29\rightarrow29$ \\
            \hline
            object                    & $83.424\pm1.422$s     &                     & $83.424\pm1.422$s  &                       & $46\rightarrow10$ & $62\rightarrow8$  & $29\rightarrow29$ \\
            \hline
            predicate                 & $427.928\pm123.118$s  &                     &                    & $427.928\pm123.118$s  & $46\rightarrow41$ & $62\rightarrow62$ & $29\rightarrow9$  \\
            \hline
            predicate (truth)         & $1594.830\pm233.849$s &                     &                    & $1594.830\pm233.849$s & $46\rightarrow5$  & $62\rightarrow62$ & $29\rightarrow6$  \\
            \hline
            predicate (falsity)       & $124.378\pm12.732$s   &                     &                    & $124.377\pm12.731$s   & $46\rightarrow11$ & $62\rightarrow62$ & $29\rightarrow9$  \\
            \hline
            action, object, predicate & $333.605\pm18.559$s   & $246.430\pm14.675$s & $84.850\pm3.892$s  & $2.325\pm0.127$s      & $46\rightarrow4$  & $62\rightarrow13$ & $29\rightarrow4$  \\
            \hline
            action, predicate, object & $313.533\pm7.165$s    & $263.337\pm10.606$s & $7.118\pm0.798$s   & $43.077\pm4.134$s     & $46\rightarrow2$  & $62\rightarrow6$  & $29\rightarrow4$  \\
            \hline
            object, action, predicate & $89.107\pm3.503$s     & $0.819\pm0.074$s    & $85.816\pm3.349$s  & $2.472\pm0.389$s      & $46\rightarrow2$  & $62\rightarrow8$  & $29\rightarrow4$  \\
            \hline
            object, predicate, action & $94.215\pm11.503$s    & $0.531\pm0.210$s    & $90.811\pm10.593$s & $2.872\pm0.754$s      & $46\rightarrow2$  & $62\rightarrow8$  & $29\rightarrow5$  \\
            \hline
            predicate, action, object & $552.037\pm46.078$s   & $324.719\pm28.436$s & $8.549\pm0.600$s   & $218.769\pm18.650$s   & $46\rightarrow2$  & $62\rightarrow7$  & $29\rightarrow9$  \\
            \hline
            predicate, object, action & $255.320\pm25.537$s   & $0.372\pm0.028$s    & $34.736\pm0.914$s  & $220.212\pm24.814$s   & $46\rightarrow2$  & $62\rightarrow7$  & $29\rightarrow9$  \\
            \hline
        \end{tabular}
        }
        \caption{Average results from 5 runs of each selected configuration of the minimizer with the task from issue 335.}
        \label{tab:avg-results-issue335}
    \end{tiny}
\end{table}

Between runs with the same delete option configuration, we did not observe different minimization outcomes.
The results do not make it obvious, which order of delete options is ideal.
If we define the sum of the remaining number of task elements as a metric for the minimization success, e.g., 21 for \textit{action, object, predicate}, we can discuss the tradeoff between the total minimization duration and how minimized the task becomes.
For this use-case, delete option configuration \textit{action, predicate, object} yields the best overall minimization result (12, according to the just defined metric) at an average running time of ${\sim}314$s.
However, configuration \textit{object, action, predicate} yields a slightly worse minimization result (14), but at less than a third of the time, ${\sim}89$s on average.
At this point, it is up to the user of the minimizer to decide which of these properties is more important.
Overall, it is pleasing to see how much the size of the task could be reduced by the minimizer, while still triggering the same bug.

\section{Use-Case 2: \textit{Segmentation Fault\\ with operator-counting heuristic and state equation contraints}}\label{sec:use-case-2:-segmentation-fault}
In August 2020, a bug report with the above title was issued in the Fast Downward public mailing list~\cite{segfault2020}.
The author implemented a Petri Net translation from PDDL and obtained a segmentation fault from Fast Downward's search component when using the following two heuristics options with the $\text{A}^{\star}$ algorithm:

\begin{itemize}
    \item operatorcounting(constraint\_generators=[state\_equation\_constraints()])
    \item operatorcounting(constraint\_generators=[state\_equation\_constraints(),lmcut\_constraints()])
\end{itemize}

In this use-case analysis, we only consider the first of the two heuristics options to reproduce the bug and test the minimizer.
With the provided $\text{SAS}^+$ file \texttt{output\_petri\_sokobanp01.sas}, we are able to reproduce the bug.
The initially parsed $\text{SAS}^+$ task instance contains 1536 operators and 184 variables.
Since this bug was issued on 01/08/2020, the most recent Fast Downward release (20.06) can be used to reproduce it.
Specifically, we are using the Git tag \texttt{release-20.06.0} with commit hash \texttt{3a27ea77f[...]} from 26/07/2020 for the experiments.
In order to run this use-case's configuration, an LP solver is required.
We use IBM's CPLEX for all test runs and experiments.

Executing the Fast Downward search component with the problem from above yields the following output:

\begin{lstlisting}[label={lst:lstlisting2},frame=single]
[t=0.000440241s, 45108 KB] reading input...
[t=0.0233119s, 46304 KB] done reading input!
[t=0.0252104s, 47280 KB] Initializing constraints from state equation.
[t=0.0392132s, 51380 KB] Building successor generator...done!
[t=0.0426499s, 51380 KB] peak memory difference for successor generator creation: 0 KB
[t=0.0426796s, 51380 KB] time for successor generation creation: 0.00317523s
[t=0.0427079s, 51380 KB] Variables: 184
[t=0.0427337s, 51380 KB] FactPairs: 368
[t=0.0427552s, 51380 KB] Bytes per state: 24
[t=0.0428768s, 51380 KB] Conducting best first search with reopening closed nodes, (real) bound = 2147483647
[t=0.0543639s, 53500 KB] New best heuristic value for operatorcounting(constraint_generators = list(state_equation_constraints)): 1
[t=0.0544395s, 53500 KB] g=0, 1 evaluated, 0 expanded
[t=0.0544747s, 53500 KB] f = 1, 1 evaluated, 0 expanded
[t=0.0545164s, 53500 KB] Initial heuristic value for operatorcounting(constraint_generators = list(state_equation_constraints)): 1
[t=0.0545561s, 53500 KB] pruning method: none
Peak memory: 53500 KB
caught signal 11 -- exiting
Segmentation fault (core dumped)
\end{lstlisting}

Again, we can use the last line of the error output (\textit{Segmentation fault (core dumped)}) as characteristic string for the minimizer.

The following command is used for our experiment runs:

\begin{lstlisting}[label={lst:command-tests-seg-fault},frame=single]
python3 /path/to/minimizer.py --problem "/path/to/downward/builds/release/bin/downward --search 'astar(operatorcounting(constraint_generators=[state_equation_constraints()]))' --internal-plan-file sas_plan < /path/to/output_petri_sokobanp01.sas" --characteristic "Segmentation fault (core dumped)" --delete variable operator
\end{lstlisting}

It turns out that after a few runs of this configuration and \textit{operator} as delete option, we are able to observe what appear to be non-deterministic results for the minimized task: after some runs, there are 18 operators remaining and after some, there are 35.
After narrowing down where these two types of runs start to differ, we are able to observe the following error output:

\begin{lstlisting}[label={lst:lstlisting3},frame=single]
[t=0.000508709s, 45112 KB] reading input...
[t=0.0125704s, 45788 KB] done reading input!
[t=0.0144455s, 46848 KB] Initializing constraints from state equation.
[t=0.0222302s, 49184 KB] Building successor generator...done!
[t=0.0237845s, 49184 KB] peak memory difference for successor generator creation: 0 KB
[t=0.0238248s, 49184 KB] time for successor generation creation: 0.00135768s
[t=0.0238734s, 49184 KB] Variables: 184
[t=0.0238988s, 49184 KB] FactPairs: 368
[t=0.0239504s, 49184 KB] Bytes per state: 24
[t=0.0240734s, 49184 KB] Conducting best first search with reopening closed nodes, (real) bound = 2147483647
realloc(): invalid pointer
Peak memory: 50364 KB
caught signal 6 -- exiting
Aborted (core dumped)
\end{lstlisting}

We believe this behavior is caused by a race condition, in which one of the signals comes from the LP solver and the other one from the system and the one reaching the planner first causes the error.
This would explain the non-deterministic nature of the result.
However, we do not investigate this issue any further for this project.
Instead, we treat these varying minimization results like the recorded running times and take their averages and standard deviations, as can be seen in Table~\ref{tab:avg-results-seg-fault}.\par
While all four running configurations in Table~\ref{tab:avg-results-seg-fault} show pleasing results in the minimization of the task elements, configuration \textit{variable, operator} stands out, reducing the initial $\text{SAS}^+$ task to only 2 variables and 2 operators.
To investigate whether the minimized task with 2 variables and 2 operators still triggers the same bug as the original task, we can run the particular Fast Downward version with GDB, The GNU Project Debugger~\cite{gdb}, separately, and compare the outputs.
The fact that these outputs are nearly identical gives us reason to believe that the same bug is still triggered with the minimized task.\par
Another possibility would be to treat both error outputs as qualifying characteristic strings, which can be achieved implementing the parser interface the following way, for example:

\begin{lstlisting}[label={lst:lstlisting3},frame=single]
from parserbase import ParserBase

class Parser(ParserBase):
    def parse_output_string(self, output_string) -> bool:
        cond1 = "caught signal 11 -- exiting" in output_string
        cond2 = "caught signal 6 -- exiting" in output_string
        return cond1 or cond2
\end{lstlisting}

That way, one would likely always obtain the same number of remaining operators after minimization.

\begin{table}[H]
    \centering
    \begin{scriptsize}
        \makebox[\textwidth]{
        \begin{tabular}{| *{6}{c|}}
            \hline
            & & \multicolumn{2}{c|}{time per option} & \multicolumn{2}{c|}{$\text{before}\rightarrow\text{after}$} \\
            delete option(s)   & total time          & operator            & variable          & operators                    & variables                  \\
            \hline
            \hline
            operator           & $478.437\pm13.088$s & $478.437\pm13.088$s &                   & $1536\rightarrow24.8\pm9.3$  & $184\rightarrow184$        \\
            \hline
            variable           & $58.908\pm2.208$s   &                     & $58.908\pm2.209$s & $1536\rightarrow432$         & $184\rightarrow2$          \\
            \hline
            operator, variable & $487.156\pm25.638$s & $451.564\pm22.753$s & $35.591\pm3.306$s & $1536\rightarrow25.6\pm10.4$ & $184\rightarrow12.2\pm6.9$ \\
            \hline
            variable, operator & $138.751\pm4.799$s  & $78.822\pm1.286$s   & $59.928\pm5.866$s & $1536\rightarrow2$           & $184\rightarrow2$          \\
            \hline
        \end{tabular}
        }
        \caption{Average results from 5 runs of each configuration of the minimizer with the task from the Segmentation Fault bug report.}
        \label{tab:avg-results-seg-fault}
    \end{scriptsize}
\end{table}



\section{Use-Case 3: Issue 736}\label{sec:use-case-3:-issue-736}
This use-case from September 2017 is another issue borrowed from the official Fast Downward issue list~\cite{issue736}.
Due to a bug in the translator, the attached planning task with files \texttt{domain.pddl} and \texttt{problem.pddl} is claimed to be unsolvable, despite there being an existing solution.
For this use-case, we make use of the available option of passing a second planner to the minimizer with its separate characteristic.
The idea is to pass a recent version of Fast Downward (commit \texttt{e9c2370e6[...]} from 27/07/2020) as reference planner, since it finds a plan for the task.
Each transformed task instance then has to remain unsolvable with the first planner while at the same time remain solvable with the second one.
The characteristic strings we want to look for in transformed task instances can be set to ``\textit{Search stopped without finding a solution.}'' for the bug inducing version of the planner and ``\textit{Solution found.}'' for the recent one.

We use the following command for our experiment runs:

\begin{lstlisting}[label={lst:command-tests-issue736},frame=single]
python3 /path/to/minimizer.py --problem "/path/to/downward/./fast-downward.py /path/to/domain736.pddl /path/to/problem736.pddl --search 'astar(blind())'" "/path/to/downward_20.06/./fast-downward.py /path/to/domain736.pddl /path/to/problem736.pddl --search 'astar(blind())'" --characteristic "Search stopped without finding a solution." "Solution found." --delete predicate object action
\end{lstlisting}

For the more common case, where no working version of the same planner is available, any other planner, for which is known that it solves the task, could be used as the reference planner.
We only tested the minimizer with Fast Downward.
Table~\ref{tab:avg-results-issue736} illustrates the running times and minimization results for a set of running configurations and the two-planner setup described above.

\begin{table}[H]
    \centering
    \begin{tiny}
        \makebox[\textwidth]{
        \begin{tabular}{| *{8}{c|}}
            \hline
            & & \multicolumn{3}{c|}{time per option} & \multicolumn{3}{c|}{$\text{before}\rightarrow\text{after}$} \\
            delete option(s)          & total time       & action           & object           & predicate        & actions         & objects         & predicates      \\
            \hline\hline
            action                    & $0.836\pm0.012$s & $0.836\pm0.012$s &                  &                  & $3\rightarrow3$ & $5\rightarrow5$ & $6\rightarrow6$ \\
            \hline
            object                    & $1.492\pm0.064$s &                  & $1.492\pm0.064$s &                  & $3\rightarrow3$ & $5\rightarrow5$ & $6\rightarrow6$ \\
            \hline
            predicate                 & $2.753\pm0.066$s &                  &                  & $2.753\pm0.066$s & $3\rightarrow3$ & $5\rightarrow5$ & $6\rightarrow3$ \\
            \hline
            predicate (truth)         & $2.734\pm0.025$s &                  &                  & $2.734\pm0.025$s & $3\rightarrow3$ & $5\rightarrow5$ & $6\rightarrow3$ \\
            \hline
            predicate (falsity)       & $1.807\pm0.027$s &                  &                  & $1.807\pm0.028$s & $3\rightarrow3$ & $5\rightarrow5$ & $6\rightarrow6$ \\
            \hline
            action, object, predicate & $5.031\pm0.088$s & $0.868\pm0.012$s & $1.425\pm0.029$s & $2.737\pm0.065$s & $3\rightarrow3$ & $5\rightarrow5$ & $6\rightarrow3$ \\
            \hline
            action, predicate, object & $5.461\pm0.119$s & $0.854\pm0.042$s & $1.933\pm0.036$s & $2.673\pm0.060$s & $3\rightarrow3$ & $5\rightarrow3$ & $6\rightarrow3$ \\
            \hline
            object, action, predicate & $5.123\pm0.235$s & $0.929\pm0.165$s & $1.462\pm0.066$s & $2.731\pm0.062$s & $3\rightarrow3$ & $5\rightarrow5$ & $6\rightarrow3$ \\
            \hline
            object, predicate, action & $5.181\pm0.170$s & $1.030\pm0.088$s & $1.445\pm0.025$s & $2.705\pm0.085$s & $3\rightarrow1$ & $5\rightarrow5$ & $6\rightarrow3$ \\
            \hline
            predicate, action, object & $5.648\pm0.119$s & $1.016\pm0.020$s & $1.900\pm0.039$s & $2.732\pm0.071$s & $3\rightarrow1$ & $5\rightarrow3$ & $6\rightarrow3$ \\
            \hline
            predicate, object, action & $5.434\pm0.106$s & $0.933\pm0.034$s & $1.874\pm0.020$s & $2.627\pm0.078$s & $3\rightarrow1$ & $5\rightarrow3$ & $6\rightarrow3$ \\
            \hline
        \end{tabular}
        }
        \caption{Average results from 5 runs of each selected configuration of the minimizer with the task from issue 736.}
        \label{tab:avg-results-issue736}
    \end{tiny}
\end{table}

Between runs with the same delete option configuration, we did not observe different minimization outcomes.
When comparing the configurations with all three delete options, it becomes evident that the order of the options determines the level of minimization.
If we again evaluate the minimization outcomes by the sum of remaining actions, objects and predicates, the two runs with \textit{predicate} as first argument become the winning ones.
The \textit{predicate, object, action} run becomes the overall winner, due to a slightly shorter average runtime between the two.

